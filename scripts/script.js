$(document).ready(function(){

	$('.login-iframe, .popup-iframe').addClass('vanish');
	
	var gate=true;
	var another_gate=true;

	$('.div-account-controls > ul > li:not(:first-child) > a, .div-account-options > ul > li:not(:first-child) > a' ).click(function(){

		if(gate){
			$('.login-iframe').removeClass('vanish');
			
			gate=!gate;
		}
		else{
			$('.login-iframe').addClass('vanish');
			
			gate=!gate;
		}
		
	})


	$('.schedule-button').click(function(){
		if(another_gate){
			$('.popup-iframe').removeClass('vanish');
			another_gate = !another_gate;
		}
		else{
			$('.popup-iframe').addClass('vanish');
			another_gate = !another_gate;
		}
		
	});

	$('.day').click(function(e){
		$('.day').css({'background-color': '#f2f2f2'});
		$(this).css({'background-color': '#feba02'});

	})
})


